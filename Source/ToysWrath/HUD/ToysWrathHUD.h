// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once 

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "ToysWrathHUD.generated.h"

UCLASS()
class AToysWrathHUD : public AHUD
{
	GENERATED_BODY()

public:
	AToysWrathHUD();

	/** Primary draw call for the HUD */
	virtual void DrawHUD() override;

private:
	/** Crosshair asset pointer */
	class UTexture2D* CrosshairTex;

};


// Fill out your copyright notice in the Description page of Project Settings.


#include "TW_HealthComponent.h"

// Sets default values for this component's properties
UTW_HealthComponent::UTW_HealthComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UTW_HealthComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}


// Called every frame
void UTW_HealthComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

float UTW_HealthComponent::GetCurrentHealth()
{
	return Health;
}

void UTW_HealthComponent::ChangeHealthValue(float ChangeValue)
{
	ChangeValue = ChangeValue * CoefDamage;

	Health += ChangeValue;

	if (Health > HealthMax)
	{
		Health = HealthMax;
	}
	else
	{

		if (Health <= 0.0f)
		{
			OnHealthDead.Broadcast();

		}
	}

	OnHealthChange.Broadcast(Health, ChangeValue);
}

void UTW_HealthComponent::SetCurrentHealth(float NewHealth)
{
	Health = NewHealth;
}


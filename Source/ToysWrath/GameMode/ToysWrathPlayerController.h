// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "ToysWrathPlayerController.generated.h"

/**
 * 
 */
UCLASS()
class TOYSWRATH_API AToysWrathPlayerController : public APlayerController
{
	GENERATED_BODY()
	
};

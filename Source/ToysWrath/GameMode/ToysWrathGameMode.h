// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "ToysWrathGameMode.generated.h"

UCLASS(minimalapi)
class AToysWrathGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AToysWrathGameMode();
};




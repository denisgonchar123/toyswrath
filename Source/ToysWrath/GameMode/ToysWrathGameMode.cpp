// Copyright Epic Games, Inc. All Rights Reserved.

#include "ToysWrathGameMode.h"
#include "../HUD/ToysWrathHUD.h"
#include "../Character/ToysWrathCharacter.h"
#include "UObject/ConstructorHelpers.h"


AToysWrathGameMode::AToysWrathGameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/ToysWarth/Character/Character/BP_Character"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = AToysWrathHUD::StaticClass();
}
